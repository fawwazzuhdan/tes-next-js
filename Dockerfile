FROM node:18-alpine

RUN apk add --no-cache libc6-compat
WORKDIR /app

COPY . .

RUN npm install

ARG SPACE_ID
ARG TOKEN

ARG ACCESS_TOKEN
ARG PREVIEW_TOKEN

RUN CONTENTFUL_SPACE_ID=$SPACE_ID CONTENTFUL_MANAGEMENT_TOKEN=$TOKEN npm run setup

COPY .env.local.example .env.local

RUN sed -i 's/###SPACE_ID###/$SPACE_ID/g' .env.local
RUN sed -i 's/###ACCESS_TOKEN###/$ACCESS_TOKEN/g' .env.local
RUN sed -i 's/###PREVIEW_TOKEN###/$PREVIEW_TOKEN/g' .env.local

RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start"]